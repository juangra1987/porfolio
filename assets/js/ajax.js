
const handleSubmit = (e) => {
  e.preventDefault()
  let myForm = document.getElementById('myFormNetlify');
  let formData = new FormData(myForm)
  fetch('/', {
    method: 'POST',
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    body: new URLSearchParams(formData).toString(),
})
    .then(() => document.querySelector ("#panel").innerHTML = "Mensaje enviado!!")
    .then( setTimeout( () => { document.querySelector ("#panel").innerHTML = "" ; document.querySelector ("#myFormNetlify").reset() } , 5000 ) )
  .catch((error) => alert(error));
}
document.querySelector("form").addEventListener("submit", handleSubmit);
